import express from 'express'
import Middleware from '../../middlewares'
import PostValidator from '../../validators/PostValidator'
import PostController from '../../controllers/PostController'

const router = express.Router()

router.get('/posts', PostController.get)
router.post('/posts', [PostValidator.create(), Middleware.Auth], PostController.create)
router.patch('/posts/:id', PostController.update)
router.delete('/posts/:id', PostController.delete)

export default router
